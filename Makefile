CC=clang
CFLAGS=-Wall -g -o
NAME=md5
VFLAGS=--tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes

all: compile run

compile:
	$(CC) $(CFLAGS) $(NAME) $(NAME).c

run:
	./$(NAME)

clean:
	rm -f $(NAME)

valgrind: compile
	valgrind $(VFLAGS) ./$(NAME)
